<?php

namespace Nohut\Providers;

use Nohut\Console\Setup\Mysql;
use Nohut\Console\Setup\Remote;
use Nohut\Controllers\Kernel;
use Nohut\Foundation\Contra;
use Nohut\Foundation\Helper;
use Nohut\Models\User;
use Illuminate\Contracts\Http\Kernel as HttpKernel;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class NohutServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Mysql::class,
                Remote::class,
            ]);
        }

        App::setLocale('tr');

        $this->loadMigrationsFrom(__DIR__.'/../Migrations/mysql');
        $this->loadTranslationsFrom(__DIR__.'/../Translations', 'nohut');
        $this->loadViewsFrom(__DIR__.'/../Resources/views', 'nohut');
        
        $this->publishes([
            base_path('vendor/monoge/nohut/src/Resources/assets') => public_path('assets'),
        ], 'nohut');

        config(['auth.providers.users.model' => User::class]);
    }

    public function register()
    {

        $this->mergeConfigFrom(
            __DIR__ . '/../Config/auth.php', 'app'
        );

        $this->app->singleton(HttpKernel::class, Kernel::class);

        $this->app->register(RouteServiceProvider::class);

        $loader = AliasLoader::getInstance();
        $loader->alias('Helper',Helper::class);
        $loader->alias('Contra',Contra::class);

    }
}
