<?php
// Namespace: Nohut\Controllers\Web

Route::get('/','HomeController@index')->name('home');

Route::prefix('auth')->group(function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login')->name('login.post');
    //Route::get('forgot-password', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('forgot-password');
    //Route::post('forgot-password', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('forgot-password.post');
    //Route::get('reset-password', 'Auth\ResetPasswordController@showResetForm')->name('reset-password');
    //Route::post('reset-password', 'Auth\ResetPasswordController@reset')->name('reset-password.post');
    Route::get('logout', 'Auth\LoginController@logoutCustom')->name('logout');
});