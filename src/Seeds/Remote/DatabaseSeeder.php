<?php

namespace Nohut\Seeds\Remote;

use Nohut\Models\UserGroup;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        UserGroup::create([
            'name'  => 'Yönetici',
        ]);
    }
}
