<?php

namespace Nohut\Seeds\Mysql;

use Nohut\Models\Currency;
use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::create([
            'code' => 'TRY',
            'define' => 'TL',
            'symbol' => '₺',
            'show' => 'define',
            'decimal_section'=>2,
            'decimal_place'=>',',
            'thousandths_place'=>'.',
            'value'=>1,
            'status'=>1
        ]);

        Currency::create([
            'code' => 'USD',
            'define' => 'USD',
            'symbol' => '$',
            'show' => 'symbol',
            'decimal_section'=>2,
            'decimal_place'=>'.',
            'thousandths_place'=>',',
            'value'=>1,
            'status'=>1
        ]);

        Currency::create([
            'code' => 'EUR',
            'define' => 'EURO',
            'symbol' => '€',
            'show' => 'symbol',
            'decimal_section'=>2,
            'decimal_place'=>'.',
            'thousandths_place'=>',',
            'value'=>1,
            'status'=>1
        ]);

        Currency::create([
            'code' => 'GBP',
            'define' => 'GBP',
            'symbol' => '£',
            'show' => 'symbol',
            'decimal_section'=>2,
            'decimal_place'=>'.',
            'thousandths_place'=>',',
            'value'=>1,
            'status'=>1
        ]);

        Currency::create([
            'code' => 'RUB',
            'define' => 'RUB',
            'symbol' => '₽',
            'show' => 'symbol',
            'decimal_section'=>2,
            'decimal_place'=>'.',
            'thousandths_place'=>',',
            'value'=>1,
            'status'=>1
        ]);

        dump('# Para birimleri oluşturuldu.');
    }
}
