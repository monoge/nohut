<?php

namespace Nohut\Seeds\Mysql;

use Nohut\Models\Language;
use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        Language::create([
            'code'  => 'tr',
            'name'  => 'Turkish',
            'native_name'   => 'Türkçe',
            'charset'   => 'UTF-8',
            'sort'      => 1,
        ]);

        Language::create([
            'code'  => 'en',
            'name'  => 'English',
            'native_name'   => 'İngilizce',
            'charset'   => 'UTF-8',
            'sort'      => 2,
        ]);

        Language::create([
            'code'  => 'de',
            'name'  => 'Deutsch',
            'native_name'   => 'Almanca',
            'charset'   => 'UTF-8',
            'sort'      => 3,
        ]);

        dump('# Diller oluşturuldu.');
    }
}
