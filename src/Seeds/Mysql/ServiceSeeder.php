<?php

namespace Nohut\Seeds\Mysql;

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // N11
        $n11 = [
            'name' => 'Mağaza Adı',
            'app_key' => 'API Anahtarı',
            'app_secret' => 'API Şifresi'
        ];

        \DB::table('services')
            ->insert([
                'type'      => 'marketplace',
                'short'     => 'N11',
                'domain'    => 'N11.COM',
                'key'      => 'n11',
                'name'      => 'N11',
                'color'     => 'red',
                'rules'     => json_encode($n11),
                'logo_xs'   => 'n11/xs.png',
                'logo_s'    => 'n11/s.png',
                'logo_m'    => 'n11/m.png',
                'order'     => 1,
                'secret'    => 0,
                'status'    => 1
            ]);

        // GITTIGIDIYOR
        $gg = [
            'name' => 'Mağaza Adı',
            'merchant_id' => 'Merchant ID',
        ];

        \DB::table('services')
            ->insert([
                'type'      => 'marketplace',
                'short'     => 'GG',
                'domain'    => 'GITTIGIDIYOR.COM',
                'key'       => 'gittigidiyor',
                'name'      => 'GITTIGIDIYOR',
                'color'     => 'green',
                'rules'     => json_encode($gg),
                'logo_xs'   => 'gg/xs.png',
                'logo_s'    => 'gg/s.png',
                'logo_m'    => 'gg/m.png',
                'order'     => 2,
                'secret'    => 0,
                'status'    => 1
            ]);

        // HEPSIBURADA
        $hb = [
            'name' => 'Mağaza Adı',
            'merchant_id' => 'Merchant ID',
        ];

        \DB::table('services')
            ->insert([
                'type'      => 'marketplace',
                'short'     => 'HB',
                'domain'    => 'HEPSIBURADA.COM',
                'key'       => 'hepsiburada',
                'name'      => 'HEPSIBURADA',
                'color'     => 'orange',
                'rules'     => json_encode($hb),
                'logo_xs'   => 'hb/xs.png',
                'logo_s'    => 'hb/s.png',
                'logo_m'    => 'hb/m.png',
                'order'     => 3,
                'secret'    => 0,
                'status'    => 1
            ]);

    }
}
