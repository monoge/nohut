<?php

namespace Nohut\Seeds\Mysql;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(LanguageTableSeeder::class);
        $this->call(CurrencyTableSeeder::class);
        $this->call(ServiceSeeder::class);

        \DB::connection()->table('servers')
            ->insert([
                'type'          => 'database',
                'slug'          => 'falcon',
                'ip'            => 'mysql',
                'port'          => 3306,
                'login'         => 'root',
                'password'      => 'Root2018!',
                'limit'         => 250,
                'status'        => 1
            ]);

    }
}
