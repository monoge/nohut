<?php

namespace Nohut\Foundation;

class Helper
{

    public static function url( $url = '/' )
    {
        return url($url);
    }

    public static function asset( $url = '/' )
    {
        return '/assets/'.$url;
    }

    public static function decrypt( $value = null, $ajax = true )
    {
        try {
            $value = decrypt($value);
        } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
            return $ajax ? false : abort(404);
        }

        return $value;
    }

}
