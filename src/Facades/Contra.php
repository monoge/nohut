<?php

namespace Nohut\Facades;

use Illuminate\Support\Facades\Facade;

class Contra extends Facade
{

    protected static function getFacadeAccessor() { return 'Contra'; }

}
