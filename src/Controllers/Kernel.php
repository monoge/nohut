<?php

namespace Nohut\Controllers;

use Nohut\Middleware\PanelAuthenticate;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{

    protected $routeMiddleware = [
        'panel.auth' => PanelAuthenticate::class,
    ];

}
