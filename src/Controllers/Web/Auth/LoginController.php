<?php

namespace Nohut\Controllers\Web\Auth;

use Nohut\Controllers\Web\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Nohut\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function showLoginForm()
    {
        return view('nohut::web.auth.login');
    }

    protected function validateLogin(Request $request)
    {
        $request = $request->all();

        $validate = Validator::make($request, [
            'email'     => 'required|email|min:6|max:255',
            'password'  => 'required|string|min:6|max:50',
        ]);

        if($validate->fails()){
            return response()->json(trans('nohut::auth.failed'),422);
        }
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        return response()->json(trans('nohut::auth.failed'),422);
    }

    public function logoutJson(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return response()->json(trans('trans::auth.status_false'),422);
    }

    public function logoutCustom($monster)
    {
        return $this->logout($monster->request);
    }

    public function check_email($monster)
    {
        $email = $monster->request->get('email');
        if( !$email ){
            return response()->json(null,404);
        }

        $request = [
            'email' => $email,
        ];

        $validate = Validator::make($request, [
            'email' => 'required|email|min:6|max:255',
        ]);

        if( $validate->fails() ){
            return response()->json(null,422);
        }

        $user = User::whereNull('deleted_at')
            ->where('status',1)
            ->where('email',$email)
            ->first();

        if( $user ){
            return response()->json(1,200); //true
        }else{
            return response()->json(0,200); //false
        }

    }

}
