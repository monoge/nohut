<?php

namespace Nohut\Controllers\Web\Auth;

use Nohut\Controllers\Web\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        return view('nohut::auth.forgot-password');
    }

    public function sendResetLinkEmail($request)
    {

        $this->validate($request, ['email' => 'required|email|exists:users'],[
            'email.required' => ':attribute bölümünü doldurmalısınız.',
            'email.email' => ':attribute geçerli değil.',
            'email.exists' => ':attribute sistemimizde kayıtlı değil.'
        ],[
            'email' => 'E-posta Adresiniz'
        ]);

        try {
            $response = $this->broker()->sendResetLink($request->only('email'));
        } catch (\Exception $e){
            return response()->json(['errors' => ['Bir hata oluştu, lütfen iletişim bölümünden bize ulaşınız.']],422);
        }

        if($response == Password::RESET_LINK_SENT){
            return response()->json('Şifrenizi değiştirebilmeniz için size e-posta gönderdik.<br/>Lütfen e-postalarınızı kontrol ediniz.',200);
        }else{
            return response()->json(['errors' => [trans($response)]],422);
        }
    }


}
