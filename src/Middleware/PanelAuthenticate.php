<?php

namespace Nohut\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

use Illuminate\Support\Facades\Auth;

class PanelAuthenticate extends Middleware
{

    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('web.login');
        }
    }
    /*
    public function handle($request, Closure $next)
    {
        dd( Auth::user() );
        if ( !Auth::guest() )
        {
            return $next($request);
        }

        return redirect()->route('web.login');

    }*/

}
