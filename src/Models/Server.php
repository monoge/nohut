<?php

namespace Nohut\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Server extends Model
{
    use SoftDeletes;
    protected $connection='mysql';
    protected $guarded=['created_at','updated_at','deleted_at'];

    public function apps()
    {
        return $this->hasMany(App::class);
    }

}
