<?php

namespace Nohut\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CurrencyDetail extends Model
{
    use SoftDeletes;
    protected $connection='mysql';
}
