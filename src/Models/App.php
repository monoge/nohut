<?php

namespace Nohut\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class App extends Model
{
    use SoftDeletes;
    protected $connection='mysql';

    public function server()
    {
        return $this->belongsTo(Server::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

}
