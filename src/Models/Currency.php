<?php

namespace Nohut\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    use SoftDeletes;
    protected $connection='mysql';

    public function details()
    {
        return $this->hasOne(CurrencyDetail::class);
    }
}
