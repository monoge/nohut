<?php

namespace Nohut\Models;

use App\Models\App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $connection = 'mysql';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [ 'created_at', 'updated_at', 'deleted_at' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->surname}";
    }

    public function app()
    {
        return $this->belongsTo(App::class);
    }

    public function group()
    {
        return $this->belongsTo(UserGroup::class,'user_group_id','id');
    }
}
