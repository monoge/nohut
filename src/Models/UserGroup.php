<?php

namespace Nohut\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserGroup extends Model
{
    use SoftDeletes;
    protected $guarded = ['created_at','updated_at','deleted_at'];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
