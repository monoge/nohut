<?php

namespace Nohut\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppService extends Model
{
    use SoftDeletes;
    protected $connection='mysql';
    protected $guarded=['created_at','updated_at','deleted_at'];

    public function service()
    {
        return $this->belongsTo(Service::class,'service_id','id');
    }

    public function app()
    {
        return $this->belongsTo(App::class);
    }

}
