<base href="{{Helper::url()}}">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
<meta http-equiv="Content-Language" content="{{App::getLocale()}}">
<meta name="csrf-token" content="{{csrf_token()}}">

<title>{{'title'}}</title>
<meta name="description" content="{{'description'}}" />
<meta name="keywords" content="{{'keywords'}}" />
<meta name="robots" content="{{'robots'}}" />