<!DOCTYPE html>
<html lang="{{App::getLocale()}}" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    @include('nohut::web.inc.head')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    @stack('styles')
  </head>
  <body>
    @yield('master')
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script>
      $.ajaxSetup({
        cache: false,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    </script>
    @stack('scripts')
  </body>
</html>
