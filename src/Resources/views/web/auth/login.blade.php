@extends('nohut::web.inc.app')

@section('master')
    <form class="form-signin">
        @csrf
        <img class="mb-4" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="">Sign in</button>
    </form>
@endsection

@push('scripts')
    <link rel="stylesheet" href="{{ Helper::asset('web/auth/login.css') }}">
    <script type="text/javascript">
        $(document).ready(function(){
            $("form.form-signin").submit(function(e){
                e.preventDefault();
                var formUrl = "{{route('web.login.post')}}";
                var formData = $(this).serialize();
                $.post( formUrl, formData, function(result){
                    alert(result);
                }, "json")
                .fail(function(result) {
                    var message = JSON.parse(result.responseText);
                    alert(message);
                });
            });
        });
    </script>
@endpush