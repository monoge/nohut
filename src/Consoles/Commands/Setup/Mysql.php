<?php

namespace Nohut\Console\Setup;

use Illuminate\Console\Command;

class Mysql extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nohut:mysql {operation=default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Veritabanı kurulum işlemleri';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Artisan::call('migrate:fresh');
        $this->info('Migrate : OK');

        $operation = $this->argument('operation');
        if ( $operation == 'seed' ) {
            \Artisan::call('db:seed', [ '--class'=> 'Nohut\\Seeds\\Mysql\\DatabaseSeeder' ]);
            $this->info('Seed    : OK');
        }

        \Artisan::call('cache:clear');

        $this->comment('Başarılı !');
    }

}