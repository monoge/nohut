<?php

namespace Nohut\Console\Setup;

use Nohut\Models\App;
use Nohut\Models\Server;
use Illuminate\Console\Command;

class Remote extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nohut:remote {code=default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Veritabanı kurulum işlemleri';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $command='migrate:fresh';

        $arr=[
            '--path'        => 'vendor/monoge/nohut/src/Migrations/remote/',
            '--database'    => 'remote',
        ];

        if ( $this->argument('code') != 'default' ) {
            $this->appUpdate( $command, $arr, $this->argument('code') );
        } else {
            $this->appCreate( $command, $arr );
        }

    }

    public function appCreate( $command, $arr )
    {
        // Sunucu bulunuyor
        $server = Server::where('type','database')
            ->where('status',1)
            ->withCount('apps')
            ->get()
            ->filter(function ($item) {
                return $item['apps_count'] != $item['limit'];
            })
            ->sortBy('apps_count')->first();

        // Sunucu'nun mysql'inde root olarak login olunuyor
        \Config::set('database.connections.root', array(
            'driver'    => 'mysql',
            'host'      => $server->ip,
            'port'      => $server->port,
            'database'  => null,
            'username'  => $server->login,
            'password'  => $server->password,
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix'    => '',
            'strict'    => true,
            'engine'    => null,
        ));

        // Uygulama için bilgiler hazırlanıyor
        $code = $this->appCodeControl($server->id);
        $db = $code.env('CUSTOMER_DB_NAME_SUFFIX','_local');
        $pass = ucfirst($server->slug).env('CUSTOMER_DB_PASSWORD_SUFFIX','2018!');

        // Sunucuda veritabanı açılıyor
        \DB::connection('root')->statement("CREATE DATABASE `".$db."`;");
        \DB::connection('root')->statement("CREATE USER '".$db."'@'%' IDENTIFIED BY '".$pass."';");
        \DB::connection('root')->statement("GRANT ALL PRIVILEGES ON ".$db.".* TO '".$db."'@'%';");
        \DB::connection('root')->statement("FLUSH PRIVILEGES;");

        // Migration ve Seed için sunucuda açılan veritabanına bağlanılıyor
        \Config::set('database.connections.remote', array(
            'driver'    => 'mysql',
            'host'      => $server->ip,
            'port'      => $server->port,
            'database'  => $db,
            'username'  => $db,
            'password'  => $pass,
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix'    => '',
            'strict'    => true,
            'engine'    => null,
        ));

        // Migration komutu çalıştırılıyor
        \Artisan::call($command,$arr);

        // Seed komutu çalıştırılıyor
        // \Artisan::call('db:seed', [ '--class'=> 'Nohut\\Seeds\\Remote\\DatabaseSeeder' ]);

        // Ana yazılım veritabanındaki bağımlılıkları oluşturuluyor
        $app = App::where('code',$code)->select('id')->first();

        $user_group_id = \DB::connection('mysql')->table('user_groups')
            ->insertGetId([
                'app_id'    => $app->id,
                'name'      => 'Yönetici',
                'color'     => '#CC0000'
            ]);

        \DB::connection('mysql')->table('users')
            ->insert([
                'app_id'        => $app->id,
                'user_group_id' => $user_group_id,
                'language_id'   => 1,
                'name'      => 'Yönetici',
                'email'     => $code.'@mail.com',
                'password'  => \Hash::make('123123ab')
            ]);

        $this->comment('Başarılı, '.$code.' kodu ile apps tablosuna oluşturuldu !');
    }

    public function appCodeControl( $server_id, $code = null )
    {
        if( !$code ) { $code=uniqid(); }

        try {
            \DB::table('apps')
                ->insert([
                    'server_id' => $server_id,
                    'code'      => $code,
                    'status'    => 0
                ]);
        } catch (\Exception $e) {
            return $this->appCodeControl($server_id);
        }

        return $code;
    }

    public function appUpdate( $command, $arr, $code )
    {
        $app = App::where('code',$code)->first();
        if( !$app ){
            $this->info('Veritabanında '.$process.' kodlu app bulunamadı!'); exit;
        }

        $server = $app->server;

        $db     = $code.env('CUSTOMER_DB_NAME_SUFFIX','_local');
        $pass   = ucfirst($server->slug).env('CUSTOMER_DB_PASSWORD_SUFFIX','2018!');

        // Migration ve Seed için sunucudaki veritabanına bağlanılıyor
        \Config::set('database.connections.remote', array(
            'driver'    => 'mysql',
            'host'      => $server->ip,
            'port'      => $server->port,
            'database'  => $db,
            'username'  => $db,
            'password'  => $pass,
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix'    => '',
            'strict'    => true,
            'engine'    => null,
        ));

        // Migration komutu çalıştırılıyor
        \Artisan::call($command,$arr);

        $this->comment('Başarılı, '.$code.' uygulamasının veritabanı güncellendi !');
    }

}