<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('remote')->create('products', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('app_id')->unsigned()->nullable();
            $table->integer('supplier_id')->unsigned()->nullable();
            $table->string('code',100)->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(env('remote'))->dropIfExists('products');
    }
}
