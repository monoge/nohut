<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('services', function (Blueprint $table) {

            $table->increments('id');

            $table->string('type',25)->nullable();

            $table->string('short',25)->nullable();
            $table->string('domain',50)->nullable();

            $table->string('key',25)->nullable();
            $table->string('name',50)->nullable();
            $table->string('color',10)->nullable();

            $table->decimal('price', 8, 2)->default(0);
            $table->integer('range')->nullable();

            $table->longText('rules')->nullable();
            $table->longText('config')->nullable();

            $table->string('logo_xs',50)->nullable();
            $table->string('logo_s',50)->nullable();
            $table->string('logo_m',50)->nullable();

            $table->integer('order')->default(0);

            $table->tinyInteger('secret')->default(0);
            $table->tinyInteger('status')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(env('DB_CONNECTION'))->dropIfExists('services');
    }
}
