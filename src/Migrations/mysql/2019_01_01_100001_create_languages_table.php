<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('languages', function (Blueprint $table) {

            $table->increments('id');

            $table->string('code')->nullable(); // tr
            $table->string('name')->nullable(); // Turkish
            $table->string('native_name')->nullable(); // Türkçe

            $table->string('charset')->default('UTF-8')->nullable();

            $table->integer('sort');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
