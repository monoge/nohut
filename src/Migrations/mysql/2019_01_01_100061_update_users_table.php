<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->integer('user_group_id')->unsigned()->default(1);
            $table->integer('app_id')->unsigned()->default(1);
            $table->integer('language_id')->unsigned()->nullable();

            $table->string('surname',50)->nullable();

            $table->string('phone',25)->nullable();

            $table->tinyInteger('status')->nullable()->default(0);

            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->integer('user_group_id');
            $table->integer('app_id');
            $table->integer('language_id');

            $table->string('surname');

            $table->string('phone');

            $table->tinyInteger('status');

            $table->timestamp('deleted_at');

        });
    }
}
