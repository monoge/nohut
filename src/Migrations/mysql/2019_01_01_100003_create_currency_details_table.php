<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('currency_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('app_id')->unsigned()->nullable();
            $table->integer('currency_id')->unsigned()->nullable();

            $table->tinyInteger('decimal_section')->nullable()->default(2);

            $table->tinyInteger('auto')->default(1);

            $table->decimal('value',18,4)->nullable();

            $table->tinyInteger('status')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(env('DB_CONNECTION'))->dropIfExists('currency_details');
    }
}
