<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('currencies', function (Blueprint $table) {

            $table->increments('id');

            $table->string('code',5)->nullable();

            $table->string('define',10)->nullable();
            $table->string('symbol',5)->nullable();
            $table->enum('show',['define','symbol']);

            $table->tinyInteger('decimal_section')->nullable()->default(2);

            $table->string('decimal_place',2)->nullable()->default('.');
            $table->string('thousandths_place',2)->nullable()->default(',');

            $table->decimal('value',18,4)->default(0);

            $table->tinyInteger('hide')->default(0);
            $table->tinyInteger('status')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
