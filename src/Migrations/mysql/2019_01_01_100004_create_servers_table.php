<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('servers', function (Blueprint $table) {

            $table->increments('id');
            $table->string('type',25)->nullable();

            $table->string('slug',50)->nullable();
            $table->longText('detail')->nullable();

            $table->string('ip',50)->nullable();
            $table->integer('port')->default(3306);
            $table->string('login',50)->nullable();
            $table->string('password',50)->nullable();

            $table->integer('limit')->default(0)->nullable();

            $table->tinyInteger('status')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->dropIfExists('servers');
    }
}
